<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reaction extends Model
{
    protected $fillable = [
        'reaction_id', 'user_id', 'post_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }}

<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Reaction; 
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\DB;
use Validator;

class ReactionController extends Controller 
{

    public function store(Request $request)
    {
        $input = $request->all();
        $post = $request->post_id;
        $user = Auth::user()->id;
        $input['user_id'] = $user;
        $checkReaction = Reaction::where([
            ['user_id', '=', $user],
            ['post_id', '=', $post],
        ])->count(); 

        if($checkReaction > 0) {
            $oldReaction = Reaction::where([
                ['user_id', '=', $user],
                ['post_id', '=', $post],
            ])->first(); 

            if($oldReaction->reaction_id == $request->reaction_id) {
                $reaction = Reaction::destroy($oldReaction->id);
            }
            else {
                $reaction = Reaction::destroy($oldReaction->id);
                $reaction = Reaction::create($input);
            }
        }

        else {
        $reaction = Reaction::create($input);
        }
        
        return response()->json($reaction, 201);

    }

    public function fetch(Request $request)
    {

        $user = Auth::user()->id;
        $post = $request->all();
        $reaction = DB::table("reactions")->where([
            ['user_id', '=', $user],
            ['post_id', '=', $post],
        ])->get();;

        return response()->json($reaction, 201);

    }


    public function delete(Reaction $reaction)
    {
        $reaction->delete();

        return response()->json(null, 204);
    }
    
}
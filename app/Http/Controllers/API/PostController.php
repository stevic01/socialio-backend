<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Post; 
use Illuminate\Support\Facades\Auth; 
use Validator;

class PostController extends Controller 
{

    public function home()
    {
        return Post::orderBy('created_at', 'desc')->with('user')->with('comments')->with('reactions')->where('visibility', 0)->get();
    }

    public function public()
    {
        return Post::orderBy('created_at', 'desc')->with('user')->with('comments')->with('reactions')->where('visibility', 1)->get();
    }

    public function show(Post $post)
    {
        return $post;
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $input['user_id'] = Auth::user()->id;
        $post = Post::create($input);

        return response()->json($post, 201);
    }

    public function update(Request $request, Post $post)
    {
        $post->update($request->all());

        return response()->json($post, 200);
    }

    public function delete(Post $post)
    {
        $post->delete();

        return response()->json(null, 204);
    }
    
}
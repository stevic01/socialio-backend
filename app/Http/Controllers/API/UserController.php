<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;
class UserController extends Controller 
{
public $successStatus = 200;
/** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(){ 
        $check = request('email');
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')->accessToken; 
            return response()->json(['success' => $success], $this->successStatus); 
            
        } else{ 
            if(User::where('email', $check)->count() > 0){
                return response()->json(['error'=>'password'], 401); 
            }
            else {
                return response()->json(['error'=>'email'], 401); 
            }
        } 
    }

    public function logout(Request $request) {
        $user = Auth::user()->token();
        $user->revoke();
        return 'logged out';
      }
/** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'username' => 'nullable', 
            'email' => 'required|email|unique:users,email', 
            'password' => 'required', 
            'c_password' => 'required|same:password',
            'first_name' => 'required',
            'last_name' => 'required',
            'date_of_birth' => 'required',
        ]);
        if ($validator->fails()) { 
                    return response()->json(['error'=>$validator->errors()], 401);            
                }
        $input = $request->all(); 
                $input['password'] = bcrypt($input['password']); 
                $user = User::create($input); 
                $success['token'] =  $user->createToken('MyApp')-> accessToken; 
        return response()->json(['success'=>$success], $this-> successStatus); 
            }
    
    public function checkmail() {
        $check = request('email');
        if(User::where('email', $check)->count() > 0){
            return response()->json(['email'=>true], 200); 
        }
        else {
            return response()->json(['email'=>false], 401); 
        }
    }
/** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function details() 
    { 
        $user = Auth::user(); 
        return response()->json(['success' => $user], $this-> successStatus); 
    } 
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'text', 'user_id', 'visibility',
        ];


    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function comments()
    {
        return $this->HasMany('App\Comment', 'post_id')->with('user');
    }

    public function reactions() {
        return $this->hasMany('App\Reaction', 'post_id')->with('user');
    }
}

<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
Route::post('checkmail', 'API\UserController@checkmail');

Route::get('post', 'API\PostController@home');

Route::group(['middleware' => 'auth:api'], function(){

Route::get('details', 'API\UserController@details');

Route::get('logout', 'API\UserController@logout');

Route::get('post', 'API\PostController@home');
Route::get('postpublic', 'API\PostController@public');
Route::get('post/{id}', 'API\PostController@show');
Route::post('post', 'API\PostController@store');
Route::put('post/{id}', 'API\PostController@update');
Route::delete('post/{id}', 'API\PostController@delete');

Route::post('comment', 'API\CommentController@store');
Route::put('comment/{id}', 'API\CommentController@update');
Route::delete('comment/{id}', 'API\CommentController@delete');

Route::post('myreaction', 'API\ReactionController@fetch');
Route::post('reaction', 'API\ReactionController@store');
Route::delete('reaction/{id}', 'API\ReactionController@delete');

});